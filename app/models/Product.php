<?php

namespace app\models;


class Product extends AppModel
{
    public function setRecentlyViewed($id)
    {
        $recentkyViewed = $this->getAllRecentlyViewed();
        if (!$recentkyViewed) {
            setcookie('recentlyViewed', $id, time() + 3600*24, '/');
        }else{
            $recentkyViewed = explode('.', $recentkyViewed);
            if (!in_array($id, $recentkyViewed)){
                $recentkyViewed[] = $id;
                $recentkyViewed = implode('.', $recentkyViewed);
                setcookie('recentlyViewed', $recentkyViewed, time() + 3600*24, '/');
            }
        }
    }
    public function getRecentlyViewed()
    {
        if (!empty($_COOKIE['recentlyViewed'])){
            $recentlyViewed = $_COOKIE['recentlyViewed'];
            $recentlyViewed = explode('.', $recentlyViewed);
            return array_slice($recentlyViewed, -3);
        }
        return false;
    }
    public function getAllRecentlyViewed()
    {
        if (!empty($_COOKIE['recentlyViewed'])){
            return $_COOKIE['recentlyViewed'];
        }
        return false;
    }
}