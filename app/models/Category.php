<?php

namespace app\models;


use ishop\App;

class Category extends AppModel
{
    public function getIds($id)
    {
        $cats = App::$app->getProperty('cats');
        $Ids = null;
        foreach ($cats as $k => $v){
            if ($v['parent_id'] == $id) {
                $Ids .= $k . ',';
                $Ids .= $this->getIds($k);
            }
        }
        return $Ids;
    }
}