<?php if(!empty($products)): ?>
    <?php $curr = \ishop\App::$app->getProperty('currency'); ?>
        <?php foreach($products as $product): ?>
            <div class="col-md-4 product-left p-left">
                <div class="product-main simpleCart_shelfItem">
                    <a href="product/<?php echo $product->alias;?>" class="mask"><img class="img-responsive zoom-img" src="images/<?php echo $product->img;?>" alt="" /></a>
                    <div class="product-bottom">
                        <h3><?php echo $product->title;?></h3>
                        <p>Explore Now</p>
                        <h4>
                            <a data-id="<?php echo $product->id;?>" class="add-to-cart-link" href="cart/add?id=<?php echo $product->id;?>"><i></i></a> <span class=" item_price"><?php echo $curr['symbol_left'];?><?php echo $product->price * $curr['value'];?><?php echo $curr['symbol_right'];?></span>
                            <?php if($product->old_price): ?>
                                <small><del><?php echo $curr['symbol_left'];?><?php echo $product->old_price * $curr['value'];?><?php echo $curr['symbol_right'];?></del></small>
                            <?php endif; ?>
                        </h4>
                    </div>
                    <div class="srch srch1">
                        <span>-50%</span>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
        <div class="clearfix"></div>
        <p class="text-center"><?php echo count($products)?> товара(ов) из <?php echo $total;?></p>
        <?php if ($pagination->countPages > 1): ?>
            <div class="text-center">
                <?php echo $pagination;?>
            </div>
        <?php endif;?>
<?php else: ?>
    <h3>Товаров не найдено</h3>
<?php endif; ?>
