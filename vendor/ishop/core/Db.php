<?php

namespace ishop;


class Db
{
    use TSingletone;

    protected function __construct()
    {
        $db = require_once CONF . '/config_db.php';
        class_alias('\RedBeanPHP\R', '\R');
        \R::setup($db['dns'], $db['user'], $db['password']);
        if (!\R::testConnection()){
            throw new \Exception('нет подключения к базе данных', 500);
        }
        \R::freeze(true);
        if (DEBUG){
            \R::debug(true, 1);
        }
    }
}